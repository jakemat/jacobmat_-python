'''
Define a function max_of_three() that takes three numbers as arguments and returns the largest of them.
'''

x=raw_input("Value of x ? ")
y=raw_input("Value of y ? ")
z=raw_input("Value of z ? ")


import random 
def max_of_three(n1,n2,n3):
    if(n1>n2):
        if(n1>n3):
            return n1
        else:
            return n3
    elif(n2>n3):
        return n2
    else: 
        return n3
                     
print max_of_three(x,y,z)    # when test is run avoid this line and the above variable assigning         
#                  Testing
'''
for i in range(100):
    x=random.randint(1,100)
    y=random.randint(1,100)
    z=random.randint(1,100)
    largest=max_of_three(x,y,z)
    print "x= %d   y= %d   z= %d   maximum = %d " %(x,y,z,largest)
'''    
                            
