'''
Write a version of a palindrome recognizer that also accepts phrase palindromes such as "Go hang a salami I'm a lasagna hog.", "Was it a 
rat I saw?", "Step on no pets", "Sit on a potato pan, Otis", "Lisa Bonet ate no basil", "Satan, oscillate my metallic sonatas", "I roamed 
under it as a tired nude Maori", "Rise to vote sir", or the exclamation "Dammit, I'm mad!". Note that punctuation, capitalization, and 
spacing are usually ignored.
'''

string = raw_input("enter a string   ")
input_str = string.lower()      #string lower fn to convert every letter to lowercase  
 
def str_reverse(string):        #String reversal function from exercise 7
    rev=""
    x=len(string)
    while(x>0):
         rev=rev+string[x-1]
         x=x-1
    return rev

def palindrome_recogniser(string):
    str_processed=""
    for i in string:
        if i == " " or i == "." or i == "?" or i == "\'" or i == "\"" or i == "-" or i == "," or i == "&" or i == "_" or i == "<" or i == ">":
            pass
        else:
            str_processed = str_processed + i

    if(str_processed==str_reverse(str_processed)):
        return True
    else:
        return False
            
if(palindrome_recogniser(input_str)):
    print "This is a palindrome"
else:
    print "This is not a palindrome"     



