'''
Define a function is_palindrome() that recognizes palindromes (i.e. words that look the same written backwards). For example, is_palindrome
(\"radar\") should return True.
'''

# String reversal function taken from the previous code
text=raw_input("Enter a string to check for palindrome    : ")

def str_reverse(string):
    rev=""
    x=len(string)
    while(x>0):
         rev=rev+string[x-1]
         x=x-1
    return rev
         
def is_palindrome(string):
    rev=str_reverse(string)
    if(string==rev):
        return True
    else:
        return False
if(is_palindrome(text)):
    print "The string is a palindrome"
else:
    print "The string is not a palindrome"    

         
