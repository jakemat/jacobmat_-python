'''The function max() from exercise 1) and the function max_of_three() from exercise 2) will only work for two and three numbers, 
respectively. But suppose we have a much larger number of numbers, or suppose we cannot tell in advance how many they are? Write a function 
max_in_list() that takes a list of numbers and returns the largest one.
'''

s=raw_input("enter a list of numbers by giving spaces in between    ")
                                           #The input created here will be a <str>

list_of_numbers = map(int, s.split())      #Here the <str> converted to list of <int>
                                            
def max_in_list(num_list):
    largest=num_list[0]
    for i in num_list:
        if(i>largest):
            largest=i
        else:
            largest=largest
    return largest
    
    
print "The largest number of the list = ",  
print max_in_list(list_of_numbers)                     

    
# split() function explanation
'''
split() fn splits a <str> at a condition[default is " "] and returns a list. Eg:x="ab abc abcd abcde" x.split()=['ab','abc','abcd','abcde']
   x.split(" ",1)=['ab','abc abcd abcde']       x.split("c")=['ab ab',' ab','d ab','de'] 
'''
#map() function explanation
'''
The map() function applies a given function to each item of an iterable (list, tuple etc.) and returns a list of the results.
map(function, iterable..)
here map(int,s.split)= take a value from string converts into an int using int function
'''
    
