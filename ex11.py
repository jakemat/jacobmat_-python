'''
Define a function generate_n_chars() that takes an integer n and a character c and returns a string, n characters long, consisting only 
of c:s. For example, generate_n_chars(5,\"x\") should return the string \"xxxxx\". (Python is unusual in that you can actually write an 
expression 5 * \"x\" that will evaluate to \"xxxxx\". For the sake of the exercise you should ignore that the problem can be solved in this 
manner.)
'''

letter=raw_input("Enter the character to build the string:  ")
no_of_times=raw_input("How many time the character is to be repeated?  ")

#raw_input always gives a value of type <str>


def generate_n_chars(count,character):
    result=""
    temp=[]
    for i in range(int(count)):       #raw_input always gives a value of type <str> so count converted into <int>
        temp[i:]=character 
    result=result.join(temp)    
    return result
    
print generate_n_chars(no_of_times,letter)

