"""
Write a function that takes a character (i.e. a string of length 1) and returns True if it is a vowel, False otherwise.
"""

alph=raw_input("Enter an alphabet:  ")

def check_vow(alp):
    flag=False
    vowels="aeiou"
    for alphabet in vowels:
        if(alp==alphabet):
            flag = True
    if(flag == True):
        return True
    else:
        return False             

if check_vow(alph):
    print "The letter is a vowel"
else:
    print "The letter is a consonant"                      

   
