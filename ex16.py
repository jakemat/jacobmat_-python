'''
Write a function filter_long_words() that takes a list of words and an integer n and returns the list of words that are longer than n.
'''

x=raw_input("Enter a list of words separated by spaces    ")
y=raw_input("Enter the filtering value an integer    ")

word_list=x.split()   #to convert string into list of words
check_value=int(y)

def filter_long_words(list_of_words,filter_value):
    result=[]
    for i in list_of_words:
        if(len(i)>filter_value):
            result.append(i)
    return result        
print "words of length > %d are :     " %check_value,
print filter_long_words(word_list,check_value)                 

   
