'''
Define a procedure histogram() that takes a list of integers and prints a histogram to the screen. For example, histogram([4, 9, 7]) 
should print the following:
    ****
    *********
    *******
'''
#generate_n_chars() function taken from the previous exercise ex11.py

def generate_n_chars(count,character):
    result=""
    temp=[]
    for i in range(count):
        temp[i:]=character                  #In the for loop a list of characters is formed
    result=result.join(temp)                #Here the list of characters will be converted into a string, join() fn 
    return result
    
    
s=raw_input("enter a list of numbers to generate a histogram        ")
                                           #The input created here will be a<str>

list_of_numbers = map(int, s.split())      #<str> converted to list of <int>                                           
print list_of_numbers

def histogram(list_arg):
    for i in list_arg:
        print generate_n_chars(i,"*")
        
            
histogram(list_of_numbers)


# split() function explanation
'''
split() fn splits a <str> at a condition[default is " "] and returns a list. Eg:x="ab abc abcd abcde" x.split()=['ab','abc','abcd','abcde']
   x.split(" ",1)=['ab','abc abcd abcde']       x.split("c")=['ab ab',' ab','d ab','de'] 
'''
#map() function explanation
'''
The map() function applies a given function to each item of an iterable (list, tuple etc.) and returns a list of the results.
map(function, iterable..)
here map(int,s.split)= take a value from string converts into an int using int function
'''

