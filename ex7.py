'''
Define a function reverse() that computes the reversal of a string. For example, reverse(\"I am testing\") should return the string 
\"gnitset ma I\".
'''

text=raw_input("Enter a string to reverse   : ")
def str_reverse(string):
    rev=""
    x=len(string)
    while(x>0):
         rev=rev+string[x-1]
         x=x-1
    return rev
    
print "The Reversed String is    : ",
print str_reverse(text)   

