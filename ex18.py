'''
A pangram is a sentence that contains all the letters of the English alphabet at least once, for example: The quick brown fox jumps over 
the lazy dog. Your task here is to write a function to check a sentence to see if it is a pangram or not.
'''


x=raw_input("Enter a senetnce to check for Pangram:    ")
input_value = x.lower()


def is_pangram(string):
    alphabets = map(chr, range(97,123))   #list of alphabets is created
    #print alphabets
    for i in string:
        for j in alphabets:
            if(i==j):
                alphabets.pop(alphabets.index(j))   #pop() function takes an integer argument. So index(j) is used for popping the alphabet.
            else:
                pass
    if(alphabets):                         #For sequences (strings, lists, tuples)- empty sequences are false
        return False
    else:
        return True 
            
if(is_pangram(input_value)):
    print "The sentence is a pangram" 
else:
    print "The sentence is not a pangram "        
