'''
Define a function overlapping() that takes two lists and returns True if they have at least one member in common, False otherwise. You may 
use your is_member() function, or the in operator, but for the sake of the exercise, you should (also) write it using two nested for-loops.
'''

array1=["jacob","mathew","abhi","hari"]
array2=["shivan","vishnu",1]

def overlapping(list1,list2):
    flag=False
    for i in list1:
        for j in list2:
            if(i==j):
                flag=True
                break
                
    if(flag):
        return True
    else:
        return False


if(overlapping(array1,array2)):
    print "Common Elements FOUND"
else:
    print "Common Elements NOT FOUND"                        

